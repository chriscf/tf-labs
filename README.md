# Lunch & Learn Terraform Labs

Welcome to the Terraform Lunch & Learn session

This repo contains some simple terraform configuration files which you can use to follow along with the labs

**Before you get started, please have a fresh, untouched zone with Enterprise plan available & activated. This can be an LTZ or a new domain, it doesn't matter, just be prepared to break it**

You also need git and terraform installed (see further reading if you need guidance)

These instructions were tested with the following versions:

*Terraform v0.12.5  
Cloudflare Provider v1.16.1*


Unsure? Run **terraform --version**

These instructions are for Mac/Linux...sorry :)

## Setup Instructions

1) Clone This Repository & change directory to terraform-labs  
    *git clone https://gitlab.com/chriscf/tf-labs.git && cd tf-labs* 
    
2) Export your API username & password to environment variables   
    *export CLOUDFLARE_EMAIL=you@example.com*  
    *export CLOUDFLARE_TOKEN=your-api-key*  
 

2) Edit **cloudflare-vars.tf** with your favourite editor and fill in the values   
-- You will find the Org ID under Zone > Overview > scroll down >  Account ID  
-- Default Origin variable should be your Origin IP; this is to demonstrate the use of variables, and is useful if you are using ephemeral IP addresses on your origins eg with our GCP account   
-- You don't need to edit any other files  
  
3) With the files saved, initialise a new terraform config by entering command **terraform init**  
-- If all goes to plan you will see '*Terraform has been successfully initialized!'* and the Cloudflare Provider will also be set up  
  
4) Type **terraform plan** - if it shows a list of changes, all is good! You can stop here, the rest will be done in the labs. We will start with the commands you just ran, so don't worry if this doesn't make sense just yet.  

## Lab Tasks

Assuming you already ran through the setup, we will do a few basic lab tasks and walk through each of them. If you didn't get to do the setup yet, do that now

1) Type **ls -l**, we will compare shortly
2) Run **terraform plan** - study the output. Notice how it is planning on creating the zone object...but it already exists in Cloudflare! Let's fix that...
3) As your zone already exists, import it with **terraform import cloudflare_zone.myzone** ***<-zone-id->*** (note, don't edit 'myzone' - this is a hard coded object reference - so just the zone ID)
4) Run **terraform plan** again; if there are no errors type **terraform apply** - wait here while we discuss, do you notice anything?
5) Confirm the apply
6) Type **ls** - what's new? Lets talk more about the state file
7) Observe the changes in the dashboard
8) In the dashboard, delete or change any one of the settings that you set with terraform, then run **terraform plan** - what happens?  Better fix that!  
9) Some DNS Changes..  
9a) Add a new record in **cloudflare-dns-uat.tf** - just copy the existing block and use whatever dummy data you want  
9b) Let's make sure we don't affect the Production DNS so to be safe we best rename that file, so go ahead and run **mv cloudflare-dns-prod.tf cloudflare-dns-prod.tf.bak**  
9c) Run **terraform fmt** to make sure our files are nice and neat  
9d) Since we're so comfortable now with Terraform and know what's going on, just deploy that change [straight to production](https://www.youtube.com/watch?v=5p8wTOr8AbU) with **terraform apply --auto-approve**  
9e) Let's just go the DNS tab and check our new record is there. What do you see? Uh-oh...
10) In **cloudflare-base.tf** set the zone plan to free & apply the change
11) Now break it all with **terraform destroy** - what happens? We will talk about dependencies.

## Files Overview

### .gitignore

This file tells git what not to sync to the repo. It's not good practice to send the Terraform state file to version control. More on this in the session...

### cloudflare-base.tf

Basic configuration of the Cloudflare Provider & a zone definition

### cloudflare-vars.tf

Variables referenced by the configuration files

### cloudflare-dns-[uat|prod].tf

Some basic DNS entries, using variables

### cloudflare-zone-override.tf

Rapid bootstrapping with zone-override settings

## Further Reading:

https://www.terraform.io/docs/providers/cloudflare/index.html  
https://developers.cloudflare.com/terraform/getting-started/  
