variable "domain" {
  default = "<your zone name e.g. example.com>"
}

variable "org-id" {
  default = "<your org ID aka account id>"
}

variable "default-origin" {
  default = "<origin ip>"
}
