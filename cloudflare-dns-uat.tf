#UAT A Records

resource "cloudflare_record" "uat-www" {
  domain  = "${var.domain}"
  name    = "uat-www"
  value   = "5.6.7.8"
  type    = "A"
  proxied = true
}