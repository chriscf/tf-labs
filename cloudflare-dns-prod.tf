#CAA
resource "cloudflare_record" "CAA_Policy" {
  domain  = "${var.domain}"
  name    = "${var.domain}"
  type    = "CAA"
  proxied = "false"

  data = {
    flags = "0"
    tag   = "issuewild"
    value = "letsencrypt.org"
  }
}

#A Records
resource "cloudflare_record" "apex" {
  domain  = "${var.domain}"
  name    = "${var.domain}"
  value   = "${var.default-origin}"
  type    = "A"
  proxied = "true"
}

resource "cloudflare_record" "www" {
  domain  = "${var.domain}"
  name    = "www"
  value   = "${var.default-origin}"
  type    = "A"
  proxied = true
}

#CNAME Records
resource "cloudflare_record" "customer-proxy" {
  domain  = "${var.domain}"
  name    = "customer.proxy"
  value   = "proxy.fallback"
  type    = "CNAME"
  proxied = true
}
