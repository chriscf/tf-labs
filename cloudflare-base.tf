provider "cloudflare" {
  # email pulled from $CLOUDFLARE_EMAIL
  # token pulled from $CLOUDFLARE_TOKEN
  org_id = "${var.org-id}"
}

resource "cloudflare_zone" "myzone" {
  zone = "${var.domain}"
  plan = "enterprise"
}