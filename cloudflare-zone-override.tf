resource "cloudflare_zone_settings_override" "bootstrap-settings" {
  name = "${var.domain}"
  settings {
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    min_tls_version          = "1.2"
    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }

    ssl        = "full"
    tls_1_3    = "zrt"
    waf        = "on"
    websockets = "off"
  }
}